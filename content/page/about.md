---
title: About me
subtitle: This is about me, so it's time to get personal.
comments: false
---

My name is Ramon Moorlag. I have the following qualities:

- I rocked a great beard for a few years
- I teach CS
- Let's have some fun with technology. 
- Also known as; the guy who fixes computers, phones and tablets
- I like swimming

