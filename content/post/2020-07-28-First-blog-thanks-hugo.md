---
title: First blog
subtitle: The one with all the goods
date: 2020-07-28
tags: ["learning", "doing"]
bigimg: [{src: "/img/blog1.jpg", desc: "firstblogpicture"}, {src: "/img/sphere.jpg", desc: "Sphere"}, {src: "/img/hexagon.jpg", desc: "Hexagon"}]
---

Learning is fun! In this blog, I'll keep updating, I'll share all the resources I've used to get this blog from the ground. 
_LOOK_ [Markdowntutorial.com](markdowntutorial.com) is now in Dutch :-) 


<!--more-->
Cool, there is a default 404 page with this image in the default theme template :-)
![Southpark](/img/404-southpark.jpg "Nothing to see here, move along")